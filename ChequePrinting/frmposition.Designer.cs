﻿namespace ChequePrinting
{
    partial class frmposition
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmposition));
            this.label1 = new System.Windows.Forms.Label();
            this.paytox = new System.Windows.Forms.TextBox();
            this.paytoy = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.banky = new System.Windows.Forms.TextBox();
            this.bankx = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.vouchernoy = new System.Windows.Forms.TextBox();
            this.vouchernox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.chequenoy = new System.Windows.Forms.TextBox();
            this.chequenox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.datey = new System.Windows.Forms.TextBox();
            this.datex = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.amountnumbery = new System.Windows.Forms.TextBox();
            this.amountnumberx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.amounttexty = new System.Windows.Forms.TextBox();
            this.amounttextx = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnsave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pay To";
            // 
            // paytox
            // 
            this.paytox.Location = new System.Drawing.Point(122, 26);
            this.paytox.Name = "paytox";
            this.paytox.Size = new System.Drawing.Size(100, 20);
            this.paytox.TabIndex = 1;
            // 
            // paytoy
            // 
            this.paytoy.Location = new System.Drawing.Point(228, 26);
            this.paytoy.Name = "paytoy";
            this.paytoy.Size = new System.Drawing.Size(100, 20);
            this.paytoy.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(131, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "x - coordinate";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(238, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "y - coordinate";
            // 
            // banky
            // 
            this.banky.Location = new System.Drawing.Point(228, 52);
            this.banky.Name = "banky";
            this.banky.Size = new System.Drawing.Size(100, 20);
            this.banky.TabIndex = 7;
            // 
            // bankx
            // 
            this.bankx.Location = new System.Drawing.Point(122, 52);
            this.bankx.Name = "bankx";
            this.bankx.Size = new System.Drawing.Size(100, 20);
            this.bankx.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Bank";
            // 
            // vouchernoy
            // 
            this.vouchernoy.Location = new System.Drawing.Point(228, 78);
            this.vouchernoy.Name = "vouchernoy";
            this.vouchernoy.Size = new System.Drawing.Size(100, 20);
            this.vouchernoy.TabIndex = 13;
            // 
            // vouchernox
            // 
            this.vouchernox.Location = new System.Drawing.Point(122, 78);
            this.vouchernox.Name = "vouchernox";
            this.vouchernox.Size = new System.Drawing.Size(100, 20);
            this.vouchernox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 85);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Voucher No";
            // 
            // chequenoy
            // 
            this.chequenoy.Location = new System.Drawing.Point(228, 104);
            this.chequenoy.Name = "chequenoy";
            this.chequenoy.Size = new System.Drawing.Size(100, 20);
            this.chequenoy.TabIndex = 16;
            // 
            // chequenox
            // 
            this.chequenox.Location = new System.Drawing.Point(122, 104);
            this.chequenox.Name = "chequenox";
            this.chequenox.Size = new System.Drawing.Size(100, 20);
            this.chequenox.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(26, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Cheque No";
            // 
            // datey
            // 
            this.datey.Location = new System.Drawing.Point(228, 130);
            this.datey.Name = "datey";
            this.datey.Size = new System.Drawing.Size(100, 20);
            this.datey.TabIndex = 19;
            // 
            // datex
            // 
            this.datex.Location = new System.Drawing.Point(122, 130);
            this.datex.Name = "datex";
            this.datex.Size = new System.Drawing.Size(100, 20);
            this.datex.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(26, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Date";
            // 
            // amountnumbery
            // 
            this.amountnumbery.Location = new System.Drawing.Point(228, 156);
            this.amountnumbery.Name = "amountnumbery";
            this.amountnumbery.Size = new System.Drawing.Size(100, 20);
            this.amountnumbery.TabIndex = 22;
            // 
            // amountnumberx
            // 
            this.amountnumberx.Location = new System.Drawing.Point(122, 156);
            this.amountnumberx.Name = "amountnumberx";
            this.amountnumberx.Size = new System.Drawing.Size(100, 20);
            this.amountnumberx.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(26, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Amount Number";
            // 
            // amounttexty
            // 
            this.amounttexty.Location = new System.Drawing.Point(228, 182);
            this.amounttexty.Name = "amounttexty";
            this.amounttexty.Size = new System.Drawing.Size(100, 20);
            this.amounttexty.TabIndex = 25;
            // 
            // amounttextx
            // 
            this.amounttextx.Location = new System.Drawing.Point(122, 182);
            this.amounttextx.Name = "amounttextx";
            this.amounttextx.Size = new System.Drawing.Size(100, 20);
            this.amounttextx.TabIndex = 24;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(26, 189);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Amount Text";
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(228, 209);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(99, 23);
            this.btnsave.TabIndex = 26;
            this.btnsave.Text = "Save Changes";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // frmposition
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 260);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.amounttexty);
            this.Controls.Add(this.amounttextx);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.amountnumbery);
            this.Controls.Add(this.amountnumberx);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.datey);
            this.Controls.Add(this.datex);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chequenoy);
            this.Controls.Add(this.chequenox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.vouchernoy);
            this.Controls.Add(this.vouchernox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.banky);
            this.Controls.Add(this.bankx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.paytoy);
            this.Controls.Add(this.paytox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmposition";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Position";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox paytox;
        private System.Windows.Forms.TextBox paytoy;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox banky;
        private System.Windows.Forms.TextBox bankx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox vouchernoy;
        private System.Windows.Forms.TextBox vouchernox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox chequenoy;
        private System.Windows.Forms.TextBox chequenox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox datey;
        private System.Windows.Forms.TextBox datex;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox amountnumbery;
        private System.Windows.Forms.TextBox amountnumberx;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox amounttexty;
        private System.Windows.Forms.TextBox amounttextx;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnsave;
    }
}