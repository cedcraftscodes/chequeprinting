﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChequePrinting
{
    public partial class frmchkprinting : Form
    {
        public frmchkprinting()
        {
            InitializeComponent();

            LoadPrinters();
            LoadBanks();
            LoadVouchers();

        }

        private void LoadPrinters()
        {
            foreach (string s in PrinterSettings.InstalledPrinters)
            {
                cboavailableprinter.Items.Add(s);
            }
            PrinterSettings settings = new PrinterSettings();
            cboavailableprinter.Text = settings.PrinterName;
        }

        private void txtamount_TextChanged(object sender, EventArgs e)
        {
            richamount.Text = ConverterUtility.ConvertWholeNumberWithDecimal(txtamount.Text);

        }

        private void txtamount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
               && !char.IsDigit(e.KeyChar)
               && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point 
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }


            if (e.KeyChar == '.' && txtamount.Text.Contains('.'))
                e.Handled = true;
            if (char.IsNumber(e.KeyChar) || e.KeyChar == '.')
            {
                if (Regex.IsMatch(
                 txtamount.Text,
                 "^\\d*\\.\\d{2}$")) e.Handled = true;
            }
            else e.Handled = e.KeyChar != (char)Keys.Back;


        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                pd.PrinterSettings.PrinterName = cboavailableprinter.Text;
                pd.DefaultPageSettings.PaperSize = new PaperSize("Letter", 216, 279);
                pd.PrintPage += PrintChequeEventHandler;
                pd.Print();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Print error: " + ex.Message);
            }

        }

        private void PrintChequeEventHandler(object sender, PrintPageEventArgs e)
        {
            Font font = Properties.Settings.Default.UserFont;
            //Not Duplicate
            e.Graphics.DrawString(txtamount.Text, font, Brushes.Black, Properties.Settings.Default.AmountNumberPosition);
            e.Graphics.DrawString(richamount.Text, font , Brushes.Black, Properties.Settings.Default.AmountTextPosition);
            e.Graphics.DrawString(txtpayto.Text, font , Brushes.Black, Properties.Settings.Default.PayToPosition);
            e.Graphics.DrawString(cbobank.Text, font, Brushes.Black, Properties.Settings.Default.BankPosition);
            e.Graphics.DrawString(txtchequenumber.Text, font , Brushes.Black, Properties.Settings.Default.ChequeNoPosition);
            e.Graphics.DrawString(chequedate.Value.ToString("MM/dd/yyyy"), font, Brushes.Black, Properties.Settings.Default.DatePosition);

            if (chkduplicate.Checked)
            {
                e.Graphics.DrawString(txtvoucherno.Text, font , Brushes.Black, Properties.Settings.Default.VoucherNoPosition);
            }
        }

        private void fontsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FontDialog fd = new FontDialog();
            if(fd.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.UserFont = fd.Font;
            }
        }

        private void positionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmposition pos = new frmposition();
            pos.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void banksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmbanks banks = new frmbanks(this);
            banks.ShowDialog();
        }


        public void LoadBanks()
        {
            cbobank.Items.Clear();
            foreach(String x in Properties.Settings.Default.Banks.Cast<string>().ToArray())
            {
                cbobank.Items.Add(x);
            }
            if(cbobank.Items.Count > 0)
            {
                cbobank.SelectedIndex = 0;
            }
        }

        public void LoadVouchers()
        {
            cbovoucherid.Items.Clear();
            foreach (String x in Properties.Settings.Default.VoucherIds.Cast<string>().ToArray())
            {
                cbovoucherid.Items.Add(x);
            }
            if (cbovoucherid.Items.Count > 0)
            {
                cbovoucherid.SelectedIndex = 0;
            }
        }

        private void voucherIdsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmvoucherids voucherids = new frmvoucherids(this);
            voucherids.ShowDialog();
        }
    }
}
