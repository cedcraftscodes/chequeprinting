﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChequePrinting
{
    public partial class frmvoucherids : Form
    {
        private frmchkprinting frmchkprinting;

        public frmvoucherids(frmchkprinting frmchkprinting)
        {
            InitializeComponent();
            ListItems();
            this.frmchkprinting = frmchkprinting;
        }

        // List the current items.
        private void ListItems()
        {
            lstvoucherids.DataSource =
                Properties.Settings.Default.VoucherIds.Cast<string>().ToArray();
            lstvoucherids.SelectedIndex = -1;
        }

        private void lstvoucherids_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstvoucherids.SelectedItem == null)
                txtvoucher.Clear();
            else
                txtvoucher.Text = lstvoucherids.SelectedItem.ToString();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.VoucherIds.Add(txtvoucher.Text);
            txtvoucher.Clear();
            txtvoucher.Focus();
            ListItems();
        }

        private void btnremove_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.VoucherIds.Remove(txtvoucher.Text);
            txtvoucher.Clear();
            txtvoucher.Focus();
            ListItems();
        }

        private void frmvoucherids_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            frmchkprinting.LoadVouchers();
        }
    }
}
