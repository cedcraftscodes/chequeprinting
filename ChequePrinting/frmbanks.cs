﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace ChequePrinting
{
    public partial class frmbanks : Form
    {
        private frmchkprinting frmchkprinting;

        public frmbanks(frmchkprinting frmchkprinting)
        {
            InitializeComponent();
            ListItems();
            this.frmchkprinting = frmchkprinting;
        }

        // List the current items.
        private void ListItems()
        {
            lstbanks.DataSource =
                Properties.Settings.Default.Banks.Cast<string>().ToArray();
            lstbanks.SelectedIndex = -1;
        }

        private void lstbanks_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstbanks.SelectedItem == null)
                txtbank.Clear();
            else
                txtbank.Text = lstbanks.SelectedItem.ToString();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Banks.Add(txtbank.Text);
            txtbank.Clear();
            txtbank.Focus();
            ListItems();
        }

        private void btnremove_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Banks.Remove(txtbank.Text);
            txtbank.Clear();
            txtbank.Focus();
            ListItems();
        }

        private void frmbanks_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            frmchkprinting.LoadBanks();
        }
    }
}
