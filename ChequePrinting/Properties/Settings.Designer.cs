﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ChequePrinting.Properties {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class Settings : global::System.Configuration.ApplicationSettingsBase {
        
        private static Settings defaultInstance = ((Settings)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new Settings())));
        
        public static Settings Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 100")]
        public global::System.Drawing.Point VoucherNoPosition {
            get {
                return ((global::System.Drawing.Point)(this["VoucherNoPosition"]));
            }
            set {
                this["VoucherNoPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 120")]
        public global::System.Drawing.Point PayToPosition {
            get {
                return ((global::System.Drawing.Point)(this["PayToPosition"]));
            }
            set {
                this["PayToPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 140")]
        public global::System.Drawing.Point BankPosition {
            get {
                return ((global::System.Drawing.Point)(this["BankPosition"]));
            }
            set {
                this["BankPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 160")]
        public global::System.Drawing.Point ChequeNoPosition {
            get {
                return ((global::System.Drawing.Point)(this["ChequeNoPosition"]));
            }
            set {
                this["ChequeNoPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 180")]
        public global::System.Drawing.Point DatePosition {
            get {
                return ((global::System.Drawing.Point)(this["DatePosition"]));
            }
            set {
                this["DatePosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 200")]
        public global::System.Drawing.Point AmountNumberPosition {
            get {
                return ((global::System.Drawing.Point)(this["AmountNumberPosition"]));
            }
            set {
                this["AmountNumberPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("20, 220")]
        public global::System.Drawing.Point AmountTextPosition {
            get {
                return ((global::System.Drawing.Point)(this["AmountTextPosition"]));
            }
            set {
                this["AmountTextPosition"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Times New Roman, 9pt")]
        public global::System.Drawing.Font UserFont {
            get {
                return ((global::System.Drawing.Font)(this["UserFont"]));
            }
            set {
                this["UserFont"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute(@"<?xml version=""1.0"" encoding=""utf-16""?>
<ArrayOfString xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">
  <string>BDO</string>
  <string>BPI</string>
  <string>EastWest</string>
  <string>LandBank</string>
</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection Banks {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["Banks"]));
            }
            set {
                this["Banks"] = value;
            }
        }
        
        [global::System.Configuration.UserScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3." +
            "org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <s" +
            "tring>IRA-101</string>\r\n</ArrayOfString>")]
        public global::System.Collections.Specialized.StringCollection VoucherIds {
            get {
                return ((global::System.Collections.Specialized.StringCollection)(this["VoucherIds"]));
            }
            set {
                this["VoucherIds"] = value;
            }
        }
    }
}
