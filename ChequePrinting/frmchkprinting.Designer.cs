﻿namespace ChequePrinting
{
    partial class frmchkprinting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmchkprinting));
            this.btnprint = new System.Windows.Forms.Button();
            this.cbobank = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtvoucherno = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbovoucherid = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtchequenumber = new System.Windows.Forms.TextBox();
            this.chequedate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.richamount = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtpayto = new System.Windows.Forms.TextBox();
            this.txtamount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboavailableprinter = new System.Windows.Forms.ComboBox();
            this.chkduplicate = new System.Windows.Forms.CheckBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maintenanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fontsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.positionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.banksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.voucherIdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnprint
            // 
            this.btnprint.Location = new System.Drawing.Point(304, 346);
            this.btnprint.Name = "btnprint";
            this.btnprint.Size = new System.Drawing.Size(75, 23);
            this.btnprint.TabIndex = 2;
            this.btnprint.Text = "Print";
            this.btnprint.UseVisualStyleBackColor = true;
            this.btnprint.Click += new System.EventHandler(this.btnprint_Click);
            // 
            // cbobank
            // 
            this.cbobank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbobank.FormattingEnabled = true;
            this.cbobank.Location = new System.Drawing.Point(126, 86);
            this.cbobank.Name = "cbobank";
            this.cbobank.Size = new System.Drawing.Size(253, 21);
            this.cbobank.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Bank";
            // 
            // txtvoucherno
            // 
            this.txtvoucherno.Location = new System.Drawing.Point(126, 140);
            this.txtvoucherno.Name = "txtvoucherno";
            this.txtvoucherno.Size = new System.Drawing.Size(253, 20);
            this.txtvoucherno.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Voucher #";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Amount";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Voucher ID";
            // 
            // cbovoucherid
            // 
            this.cbovoucherid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbovoucherid.FormattingEnabled = true;
            this.cbovoucherid.Location = new System.Drawing.Point(126, 113);
            this.cbovoucherid.Name = "cbovoucherid";
            this.cbovoucherid.Size = new System.Drawing.Size(253, 21);
            this.cbovoucherid.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Cheque Number";
            // 
            // txtchequenumber
            // 
            this.txtchequenumber.Location = new System.Drawing.Point(126, 166);
            this.txtchequenumber.Name = "txtchequenumber";
            this.txtchequenumber.Size = new System.Drawing.Size(253, 20);
            this.txtchequenumber.TabIndex = 10;
            // 
            // chequedate
            // 
            this.chequedate.Location = new System.Drawing.Point(126, 193);
            this.chequedate.Name = "chequedate";
            this.chequedate.Size = new System.Drawing.Size(253, 20);
            this.chequedate.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 200);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Date";
            // 
            // richamount
            // 
            this.richamount.Location = new System.Drawing.Point(15, 244);
            this.richamount.Name = "richamount";
            this.richamount.Size = new System.Drawing.Size(364, 96);
            this.richamount.TabIndex = 14;
            this.richamount.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Pay to the Order of";
            // 
            // txtpayto
            // 
            this.txtpayto.Location = new System.Drawing.Point(126, 60);
            this.txtpayto.Name = "txtpayto";
            this.txtpayto.Size = new System.Drawing.Size(253, 20);
            this.txtpayto.TabIndex = 15;
            // 
            // txtamount
            // 
            this.txtamount.Location = new System.Drawing.Point(126, 218);
            this.txtamount.Name = "txtamount";
            this.txtamount.Size = new System.Drawing.Size(253, 20);
            this.txtamount.TabIndex = 0;
            this.txtamount.TextChanged += new System.EventHandler(this.txtamount_TextChanged);
            this.txtamount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtamount_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Printer";
            // 
            // cboavailableprinter
            // 
            this.cboavailableprinter.FormattingEnabled = true;
            this.cboavailableprinter.Location = new System.Drawing.Point(126, 33);
            this.cboavailableprinter.Name = "cboavailableprinter";
            this.cboavailableprinter.Size = new System.Drawing.Size(253, 21);
            this.cboavailableprinter.TabIndex = 17;
            // 
            // chkduplicate
            // 
            this.chkduplicate.AutoSize = true;
            this.chkduplicate.Location = new System.Drawing.Point(221, 350);
            this.chkduplicate.Name = "chkduplicate";
            this.chkduplicate.Size = new System.Drawing.Size(77, 17);
            this.chkduplicate.TabIndex = 19;
            this.chkduplicate.Text = "Duplicate?";
            this.chkduplicate.UseVisualStyleBackColor = true;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.maintenanceToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(398, 24);
            this.menuStrip1.TabIndex = 20;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // maintenanceToolStripMenuItem
            // 
            this.maintenanceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fontsToolStripMenuItem,
            this.positionToolStripMenuItem,
            this.banksToolStripMenuItem,
            this.voucherIdsToolStripMenuItem});
            this.maintenanceToolStripMenuItem.Name = "maintenanceToolStripMenuItem";
            this.maintenanceToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.maintenanceToolStripMenuItem.Text = "Maintenance";
            // 
            // fontsToolStripMenuItem
            // 
            this.fontsToolStripMenuItem.Name = "fontsToolStripMenuItem";
            this.fontsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fontsToolStripMenuItem.Text = "Fonts";
            this.fontsToolStripMenuItem.Click += new System.EventHandler(this.fontsToolStripMenuItem_Click);
            // 
            // positionToolStripMenuItem
            // 
            this.positionToolStripMenuItem.Name = "positionToolStripMenuItem";
            this.positionToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.positionToolStripMenuItem.Text = "Position";
            this.positionToolStripMenuItem.Click += new System.EventHandler(this.positionToolStripMenuItem_Click);
            // 
            // banksToolStripMenuItem
            // 
            this.banksToolStripMenuItem.Name = "banksToolStripMenuItem";
            this.banksToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.banksToolStripMenuItem.Text = "Banks";
            this.banksToolStripMenuItem.Click += new System.EventHandler(this.banksToolStripMenuItem_Click);
            // 
            // voucherIdsToolStripMenuItem
            // 
            this.voucherIdsToolStripMenuItem.Name = "voucherIdsToolStripMenuItem";
            this.voucherIdsToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.voucherIdsToolStripMenuItem.Text = "VoucherIds";
            this.voucherIdsToolStripMenuItem.Click += new System.EventHandler(this.voucherIdsToolStripMenuItem_Click);
            // 
            // frmchkprinting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 382);
            this.Controls.Add(this.chkduplicate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cboavailableprinter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtpayto);
            this.Controls.Add(this.richamount);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.chequedate);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtchequenumber);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cbovoucherid);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtvoucherno);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbobank);
            this.Controls.Add(this.btnprint);
            this.Controls.Add(this.txtamount);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmchkprinting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cheque Printing";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnprint;
        private System.Windows.Forms.ComboBox cbobank;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtvoucherno;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbovoucherid;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtchequenumber;
        private System.Windows.Forms.DateTimePicker chequedate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox richamount;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtpayto;
        private System.Windows.Forms.TextBox txtamount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cboavailableprinter;
        private System.Windows.Forms.CheckBox chkduplicate;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maintenanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fontsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem positionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem banksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem voucherIdsToolStripMenuItem;
    }
}

