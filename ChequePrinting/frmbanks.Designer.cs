﻿namespace ChequePrinting
{
    partial class frmbanks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmbanks));
            this.lstbanks = new System.Windows.Forms.ListBox();
            this.txtbank = new System.Windows.Forms.TextBox();
            this.btnadd = new System.Windows.Forms.Button();
            this.btnremove = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstbanks
            // 
            this.lstbanks.FormattingEnabled = true;
            this.lstbanks.Location = new System.Drawing.Point(12, 38);
            this.lstbanks.Name = "lstbanks";
            this.lstbanks.Size = new System.Drawing.Size(211, 212);
            this.lstbanks.TabIndex = 0;
            this.lstbanks.SelectedIndexChanged += new System.EventHandler(this.lstbanks_SelectedIndexChanged);
            // 
            // txtbank
            // 
            this.txtbank.Location = new System.Drawing.Point(12, 12);
            this.txtbank.Name = "txtbank";
            this.txtbank.Size = new System.Drawing.Size(209, 20);
            this.txtbank.TabIndex = 1;
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(227, 9);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(137, 23);
            this.btnadd.TabIndex = 2;
            this.btnadd.Text = "Add";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // btnremove
            // 
            this.btnremove.Location = new System.Drawing.Point(227, 39);
            this.btnremove.Name = "btnremove";
            this.btnremove.Size = new System.Drawing.Size(137, 23);
            this.btnremove.TabIndex = 3;
            this.btnremove.Text = "Remove";
            this.btnremove.UseVisualStyleBackColor = true;
            this.btnremove.Click += new System.EventHandler(this.btnremove_Click);
            // 
            // frmbanks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 258);
            this.Controls.Add(this.btnremove);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.txtbank);
            this.Controls.Add(this.lstbanks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmbanks";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Banks";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmbanks_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstbanks;
        private System.Windows.Forms.TextBox txtbank;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.Button btnremove;
    }
}