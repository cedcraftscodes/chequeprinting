﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChequePrinting
{
    public partial class frmposition : Form
    {
        public frmposition()
        {
            InitializeComponent();

            Point payto = Properties.Settings.Default.PayToPosition;
            paytox.Text = payto.X.ToString();
            paytoy.Text = payto.Y.ToString();


            Point bank = Properties.Settings.Default.BankPosition;
            bankx.Text = bank.X.ToString();
            banky.Text = bank.Y.ToString();

            Point voucherno = Properties.Settings.Default.VoucherNoPosition;
            vouchernox.Text = voucherno.X.ToString();
            vouchernoy.Text = voucherno.Y.ToString();



            Point chequeno = Properties.Settings.Default.ChequeNoPosition;
            chequenox.Text = chequeno.X.ToString();
            chequenoy.Text = chequeno.Y.ToString();


            Point date = Properties.Settings.Default.DatePosition;
            datex.Text = date.X.ToString();
            datey.Text = date.Y.ToString();


            Point amountnumber = Properties.Settings.Default.AmountNumberPosition;
            amountnumberx.Text = amountnumber.X.ToString();
            amountnumbery.Text = amountnumber.Y.ToString();

            Point amounttext = Properties.Settings.Default.AmountTextPosition;
            amounttextx.Text = amounttext.X.ToString();
            amounttexty.Text = amounttext.Y.ToString();
            




        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                Point payto = new Point(Convert.ToInt32(paytox.Text), Convert.ToInt32(paytoy.Text));
                Properties.Settings.Default.PayToPosition = payto;

                Point bank = new Point(Convert.ToInt32(bankx.Text), Convert.ToInt32(banky.Text));
                Properties.Settings.Default.BankPosition = bank;

                Point vouchernumber = new Point(Convert.ToInt32(vouchernox.Text), Convert.ToInt32(vouchernoy.Text));
                Properties.Settings.Default.VoucherNoPosition = vouchernumber;


                Point chequeno = new Point(Convert.ToInt32(chequenox.Text), Convert.ToInt32(chequenoy.Text));
                Properties.Settings.Default.ChequeNoPosition = chequeno;

                Point date = new Point(Convert.ToInt32(datex.Text), Convert.ToInt32(datey.Text));
                Properties.Settings.Default.DatePosition = date;


                Point amountno = new Point(Convert.ToInt32(amountnumberx.Text), Convert.ToInt32(amountnumbery.Text));
                Properties.Settings.Default.AmountNumberPosition = amountno;

                Point amounttext = new Point(Convert.ToInt32(amounttextx.Text), Convert.ToInt32(amounttexty.Text));
                Properties.Settings.Default.AmountTextPosition = amounttext;

                Properties.Settings.Default.Save();
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.Save();

                MessageBox.Show("Positions Updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid Input!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
