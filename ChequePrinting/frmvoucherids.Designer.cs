﻿namespace ChequePrinting
{
    partial class frmvoucherids
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmvoucherids));
            this.btnremove = new System.Windows.Forms.Button();
            this.btnadd = new System.Windows.Forms.Button();
            this.txtvoucher = new System.Windows.Forms.TextBox();
            this.lstvoucherids = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnremove
            // 
            this.btnremove.Location = new System.Drawing.Point(227, 39);
            this.btnremove.Name = "btnremove";
            this.btnremove.Size = new System.Drawing.Size(137, 23);
            this.btnremove.TabIndex = 7;
            this.btnremove.Text = "Remove";
            this.btnremove.UseVisualStyleBackColor = true;
            this.btnremove.Click += new System.EventHandler(this.btnremove_Click);
            // 
            // btnadd
            // 
            this.btnadd.Location = new System.Drawing.Point(227, 9);
            this.btnadd.Name = "btnadd";
            this.btnadd.Size = new System.Drawing.Size(137, 23);
            this.btnadd.TabIndex = 6;
            this.btnadd.Text = "Add";
            this.btnadd.UseVisualStyleBackColor = true;
            this.btnadd.Click += new System.EventHandler(this.btnadd_Click);
            // 
            // txtvoucher
            // 
            this.txtvoucher.Location = new System.Drawing.Point(12, 12);
            this.txtvoucher.Name = "txtvoucher";
            this.txtvoucher.Size = new System.Drawing.Size(209, 20);
            this.txtvoucher.TabIndex = 5;
            // 
            // lstvoucherids
            // 
            this.lstvoucherids.FormattingEnabled = true;
            this.lstvoucherids.Location = new System.Drawing.Point(12, 38);
            this.lstvoucherids.Name = "lstvoucherids";
            this.lstvoucherids.Size = new System.Drawing.Size(211, 212);
            this.lstvoucherids.TabIndex = 4;
            this.lstvoucherids.SelectedIndexChanged += new System.EventHandler(this.lstvoucherids_SelectedIndexChanged);
            // 
            // frmvoucherids
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 262);
            this.Controls.Add(this.btnremove);
            this.Controls.Add(this.btnadd);
            this.Controls.Add(this.txtvoucher);
            this.Controls.Add(this.lstvoucherids);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmvoucherids";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Voucher Ids";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmvoucherids_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnremove;
        private System.Windows.Forms.Button btnadd;
        private System.Windows.Forms.TextBox txtvoucher;
        private System.Windows.Forms.ListBox lstvoucherids;
    }
}